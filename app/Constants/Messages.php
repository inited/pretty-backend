<?php
/**
 * Created by PhpStorm.
 * User: pavlito
 * Date: 2.2.18
 * Time: 12:23
 */

namespace App\Constants;

/**
 * Class Messages
 * @package App\Constants
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class Messages
{

    // Default
    const ENTITY_ID_NOT_FOUND = 'Entity [ID:"%s"] was not found.';
    const FILE_NOT_FOUND = 'File was not found';
    const INPUT_NOT_FILE_OBJECT = 'Parameter ["%s"] is not a file object';
    const INVALID_CREDENTIALS = 'Invalid credentials.';
    const INVALID_EMAIL = 'Email does not have the correct format.';
    const INVALID_ONE_OR_MORE_ARGUMENTS = 'One or more input parameters are invalid.';
    const MISSING_ONE_OR_MORE_ARGUMENTS = 'All data must be filled.';

    // User
    const USER_ALREADY_EXISTS = 'User already exists.';
    const USER_NOT_ACTIVE = 'Your account is not activated yet, please wait for activation.';
    const USER_NOT_FOUND = 'User not found.';
    const USER_PASSWORD_KEY_NOT_ACCEPTABLE = 'Key not acceptable.';
    const USER_PASSWORD_NO_KEY_WAS_PROVIDED = 'No key was provided.';
    const USER_PASSWORD_REQUEST_EMPTY_EMAIL = 'You have to fill email.';
    const USER_PASSWORD_SET_MISSING_ARGUMENTS = 'Either password was not filled or password request key was not provided.';


}
