<?php
/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

use App\Constants\Messages;

/**
 * Class InvalidEmailException
 * @package App\Exceptions
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class InvalidEmailException extends \Exception
{

    protected $message = Messages::INVALID_EMAIL;
}
