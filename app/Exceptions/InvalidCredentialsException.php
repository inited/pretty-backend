<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class InvalidCredentialsException
 * @package App\Exceptions
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class InvalidCredentialsException extends \Exception
{

}
