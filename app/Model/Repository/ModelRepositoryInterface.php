<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Model;

/**
 * Interface ModelRepositoryInterface
 * @package App\Model\Repository
 */
interface ModelRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param int $id
     * @return Model
     * @throws EntityNotFoundException
     */
    public function findModel(int $id): Model;

}
