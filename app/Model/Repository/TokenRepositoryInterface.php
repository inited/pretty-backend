<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\InvalidAuthTokenException;
use App\Model\Entity\TokenEntity;
use App\Model\Entity\UserEntity;

/**
 * Interface TokenRepositoryInterface
 * @package App\Model\Repository
 */
interface TokenRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param UserEntity $entity
     * @return mixed
     */
    public function generateToken(UserEntity $entity);

    /**
     * @param TokenEntity $token
     * @return bool
     */
    public function invalidateToken(TokenEntity $token);

    /**
     * @param mixed $token
     * @return TokenEntity
     * @throws InvalidAuthTokenException
     */
    public function verifyToken($token);

}
