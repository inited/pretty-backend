<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Device;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class DeviceRepository
 * @package App\Model\Repository
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class DeviceRepository extends BaseRepository implements DeviceRepositoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function findByToken(string $token): Device
    {
        try {
            $device = $this->_em->createQueryBuilder()
                ->select('device')
                ->from(Device::class, 'device')
                ->andWhere('device.token = :token')
                ->setParameter('token', $token)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $device) {
                return $device;
            }

        } catch (NonUniqueResultException $e) {

        }

        throw new EntityNotFoundException();
    }
}
