<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 27. 02. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Notification;

/**
 * Interface NotificationRepositoryInterface
 * @package App\Model\Repository
 */
interface NotificationRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param int $id
     *
     * @return Notification
     * @throws EntityNotFoundException
     */
    public function findNotification(int $id): Notification;

    /**
     * @return array
     */
    public function getAllNotifications(): array;

}
