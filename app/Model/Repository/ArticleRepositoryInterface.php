<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Article;

/**
 * Interface ArticleRepositoryInterface
 * @package App\Model\Repository
 */
interface ArticleRepositoryInterface extends BaseRepositoryInterface
{

    /**
     * @param int $id
     * @return Article
     * @throws EntityNotFoundException
     */
    public function findArticle(int $id): Article;

    /**
     * @return array
     */
    public function findArticles(): array;

}
