<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

use App\Model\Entity\UrlEmbeddable;

/**
 * Class UrlEmbeddableValueObject
 * @package App\Model\ValueObject
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class UrlEmbeddableValueObject
{

    /**
     * @var null|integer
     */
    public $timestamp;

    /**
     * @var null|string
     */
    public $url;

    /**
     * UrlEmbeddableValueObject constructor.
     * @param UrlEmbeddable|null $urlEmbeddable
     */
    public function __construct(?UrlEmbeddable $urlEmbeddable = null)
    {
        if ($urlEmbeddable instanceof UrlEmbeddable) {
            $this->timestamp = $urlEmbeddable->getTimestamp() instanceof \DateTimeInterface ?
                $urlEmbeddable->getTimestamp()->getTimestamp() : null;
            $this->url = $urlEmbeddable->getUrl();
        }
    }
}
