<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

use App\Model\Entity\ModelGalleryItem;

/**
 * Class ModelGalleryItemValueObject
 * @package App\Model\ValueObject
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelGalleryItemValueObject
{

    /**
     * @var string
     */
    public $url;

    /**
     * @var int
     */
    public $timestamp;

    /**
     * ModelGalleryItemValueObject constructor.
     * @param ModelGalleryItem $modelGalleryItem
     * @throws \Exception
     */
    public function __construct(ModelGalleryItem $modelGalleryItem)
    {
        $this->url = $modelGalleryItem->getUrl();
        $this->timestamp = $modelGalleryItem->getTimestamp()->getTimestamp();
    }
}
