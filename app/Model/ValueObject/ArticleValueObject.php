<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

use App\Model\Entity\Article;

/**
 * Class ArticleValueObject
 * @package App\Model\ValueObject
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArticleValueObject
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $text;

    /**
     * @var int
     */
    public $timestamp;

    /**
     * ArticleValueObject constructor.
     *
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->id = $article->getId();
        $this->key = $article->getKey();
        $this->text = $article->getText();
        $this->timestamp = $article->getTimestamp()->getTimestamp();
    }
}
