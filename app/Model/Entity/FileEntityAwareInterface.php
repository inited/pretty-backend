<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

/**
 * Interface FileEntityAwareInterface
 * @package App\Model\Entity
 */
interface FileEntityAwareInterface
{

    /**
     * @param mixed $fileUrl
     * @return void
     */
    public function setServerUrl($fileUrl);
}
