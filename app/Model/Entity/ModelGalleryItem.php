<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 16. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Class GalleryItem
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Entity()
 * @ORM\Table(name="model__gallery_item")
 * @ORM\HasLifecycleCallbacks()
 */
class ModelGalleryItem
{

    /**
     * @var string
     * @ORM\Column(name="id")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var Model
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Model", inversedBy="gallery")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $model;

    /**
     * @var UrlEmbeddable
     * @ORM\Embedded(class="App\Model\Entity\UrlEmbeddable")
     */
    private $item;

    /**
     * ModelGalleryItem constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model): void
    {
        $this->model = $model;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getUrl(): string
    {
        return $this->getItem()->getUrl();
    }

    /**
     * @return UrlEmbeddable
     * @throws \Exception
     */
    public function getItem(): UrlEmbeddable
    {
        return $this->item ?? new UrlEmbeddable();
    }

    /**
     * @param UrlEmbeddable $item
     */
    public function setItem(UrlEmbeddable $item): void
    {
        $this->item = $item;
    }

    /**
     * @return \DateTimeInterface
     * @throws \Exception
     */
    public function getTimestamp(): \DateTimeInterface
    {
        return $this->getItem()->getTimestamp();
    }

}
