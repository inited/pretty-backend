<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UrlEmbeddable
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Embeddable()
 */
final class UrlEmbeddable
{

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var string
     * @ORM\Column(name="url", type="string")
     */
    private $url = '';

    /**
     * UrlEmbeddable constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * @return \DateTimeInterface
     */
    public function getTimestamp(): \DateTimeInterface
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTimeInterface $timestamp
     */
    public function setTimestamp(\DateTimeInterface $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

}
