<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Archetype
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Entity()
 * @ORM\Table(name="model__model")
 * @ORM\HasLifecycleCallbacks()
 */
final class Model
{

    const CATEGORY_SILVER = 'SILVER';
    const CATEGORY_STANDARD = 'STANDARD';

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = true;

    /**
     * @var string
     * @ORM\Column(name="buy_url")
     */
    private $buyUrl = '';

    /**
     * @var string
     * @ORM\Column(name="category")
     */
    private $category = '';

    /**
     * @var string
     * @ORM\Column(name="description", type="text")
     */
    private $description = '';

    /**
     * @var UrlEmbeddable
     * @ORM\Embedded(class="App\Model\Entity\UrlEmbeddable", columnPrefix="model_")
     */
    private $model;

    /**
     * @var ArrayCollection|ModelGalleryItem[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\ModelGalleryItem", mappedBy="model", cascade={"persist"}, orphanRemoval=true)
     */
    private $gallery;

    /**
     * @var int
     * @ORM\Column(name="`order`", type="integer")
     */
    private $order = 1;

    /**
     * @var UrlEmbeddable
     * @ORM\Embedded(class="App\Model\Entity\UrlEmbeddable", columnPrefix="picture_")
     */
    private $picture;

    /**
     * @var string
     * @ORM\Column(name="title")
     */
    private $title = '';

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var string
     * @ORM\Column(name="view_url")
     */
    private $viewUrl = '';

    /**
     * Model constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->gallery = new ArrayCollection();
        $this->updated = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     * @param PreUpdateEventArgs $event
     * @throws \Exception
     */
    public function checkEntityChangeSet(PreUpdateEventArgs $event): void
    {
        if (0 < count($event->getEntityChangeSet())) {
            $this->updated = new \DateTime();
        }

        if($event->hasChangedField('picture.url')) {
            $this->getPicture()->setTimestamp(new \DateTime());
        }

        if($event->hasChangedField('model.url')) {
            $this->getModel()->setTimestamp(new \DateTime());
        }
    }

    /**
     * @param ModelGalleryItem $modelGalleryItem
     * @throws \Exception
     */
    public function addModelGalleryItem(ModelGalleryItem $modelGalleryItem): void
    {
        $modelGalleryItem->setModel($this);
        $this->gallery->add($modelGalleryItem);
        $this->updated = new \DateTime();
    }

    public function removeModelGalleryItems(): void
    {
        foreach ($this->gallery as $item) {
            $this->gallery->removeElement($item);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdated(): \DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * @return string
     */
    public function getBuyUrl(): string
    {
        return $this->buyUrl;
    }

    /**
     * @param string $buyUrl
     */
    public function setBuyUrl(string $buyUrl): void
    {
        $this->buyUrl = $buyUrl;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return ModelGalleryItem[]
     */
    public function getGallery()
    {
        return $this->gallery->toArray();
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return UrlEmbeddable
     * @throws \Exception
     */
    public function getModel(): UrlEmbeddable
    {
        return $this->model ?? new UrlEmbeddable();
    }

    /**
     * @param UrlEmbeddable $model
     */
    public function setModel(UrlEmbeddable $model): void
    {
        $this->model = $model;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @return UrlEmbeddable
     * @throws \Exception
     */
    public function getPicture(): UrlEmbeddable
    {
        return $this->picture ?? new UrlEmbeddable();
    }

    /**
     * @param UrlEmbeddable $picture
     */
    public function setPicture(UrlEmbeddable $picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getViewUrl(): string
    {
        return $this->viewUrl;
    }

    /**
     * @param string $viewUrl
     */
    public function setViewUrl(string $viewUrl): void
    {
        $this->viewUrl = $viewUrl;
    }

}
