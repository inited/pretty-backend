<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Model;
use App\Model\Entity\ModelGalleryItem;
use App\Model\Entity\UrlEmbeddable;
use App\Model\ValueObject\ModelValueObject;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ModelCreateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelCreateController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ModelCreateController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        if (is_array($data) && count($data)) {
            $entity = new Model();

            if (array_key_exists('active', $data)) {
                $entity->setActive(filter_var($data['active'], FILTER_VALIDATE_BOOLEAN));
            }

            if (array_key_exists('buyURL', $data)) {
                $entity->setBuyUrl((string)($data['buyURL'] ?? ''));
            }

            if (array_key_exists('category', $data)) {
                $entity->setCategory((string)($data['category'] ?? ''));
            }

            if (array_key_exists('description', $data)) {
                $entity->setDescription((string)($data['description'] ?? ''));
            }

            if (array_key_exists('model', $data)) {
                $urlEmbeddable = new UrlEmbeddable();
                $urlEmbeddable->setUrl((string)($data['model']['url'] ?? ''));
                $entity->setModel($urlEmbeddable);
            }

            if (array_key_exists('gallery', $data)) {
                foreach ((array)$data['gallery'] as $item) {
                    $urlEmbeddable = new UrlEmbeddable();
                    $urlEmbeddable->setUrl((string)($item['url'] ?? ''));

                    $galleryItem = new  ModelGalleryItem();
                    $galleryItem->setItem($urlEmbeddable);

                    $entity->addModelGalleryItem($galleryItem);
                }
            }

            if (array_key_exists('order', $data)) {
                $entity->setOrder(intval($data['order']));
            }

            if (array_key_exists('picture', $data)) {
                $urlEmbeddable = new UrlEmbeddable();
                $urlEmbeddable->setUrl((string)($data['picture']['url'] ?? ''));
                $entity->setPicture($urlEmbeddable);
            }

            if (array_key_exists('title', $data)) {
                $entity->setTitle((string)($data['title'] ?? ''));
            }

            if (array_key_exists('viewURL', $data)) {
                $entity->setViewUrl((string)($data['viewURL'] ?? ''));
            }

            $this->em->persist($entity);
            $this->em->flush();

            return $response->withJson(new ModelValueObject($entity), 200);

        }

        return $response->withStatus(400);
    }

}
