<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Answers;
use App\Model\Entity\Contest;
use App\Model\ValueObject\ContestValueObject;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineExtensions\Query\Mysql\Date;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ContestCreateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ContestCreateController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * ContestCreateController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (is_array($data) && count($data)) {
            $entity = new Contest();

			if (array_key_exists('finishOn', $data)) {
				$finishOn = \DateTime::createFromFormat('U', '' . $data['finishOn']);
				if (!$finishOn){
					return $response->write('This finishOn format is not valid! See documentation');
				}

				$entity->setFinishOn($finishOn);
			}

			if (array_key_exists('created', $data)) {
				$created = \DateTime::createFromFormat('U', '' . $data['created']);
				if (!$created){
					return $response->write('This created format is not valid! See documentation');
				}

				$entity->setCreated($created);
			}

			if (array_key_exists('status', $data)) {
				if (in_array($data['status'], $this->statusAllowedTypes)){
					$entity->setStatus($data['status']);
				}else{
					return $response->write('This status is not allowed');
				}
			}

			if (array_key_exists('contestType', $data)) {
				if (in_array($data['contestType'], $this->contestAllowedTypes)){
					$entity->setContestType($data['contestType']);
				}else{
					return $response->write('This contestType is not allowed');
				}
			}

			if (array_key_exists('question', $data)) {
				$entity->setQuestion($data['question']);
			}

			if (array_key_exists('image', $data)) {
				$entity->setImage($data['image']);
			}

			if (array_key_exists('answers', $data) && is_array($data['answers'])) {

				$order = 1;
				$savedAnswers = [];

				foreach ($data['answers'] as $answer){
					$answerEntity = new Answers();
					$answerEntity->setText($answer);
					$answerEntity->setContest($entity);
					$answerEntity->setOrder($order);
					$this->em->persist($answerEntity);


					$order++;
				}
			}
			
            $this->em->persist($entity);
            $this->em->flush();
            $this->em->clear();

            /** @var Contest $contest */
            $contest = $this->em->getRepository(Contest::class)->findOneBy(['id' => $entity->getId()]);

            return $response->withJson(new ContestValueObject($contest), 200);

        }

        return $response->withStatus(400);
    }

}
