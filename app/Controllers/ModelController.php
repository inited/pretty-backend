<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\ModelRepositoryInterface;
use App\Model\ValueObject\ModelValueObject;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ModelController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelController extends DefaultController
{

    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepository;

    /**
     * ModelController constructor.
     * @param ModelRepositoryInterface $modelRepository
     */
    public function __construct(ModelRepositoryInterface $modelRepository)
    {
        $this->modelRepository = $modelRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $entity = $this->modelRepository->findModel(intval($args['id']));
                return $response->withJson(new ModelValueObject($entity), 200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }

}
