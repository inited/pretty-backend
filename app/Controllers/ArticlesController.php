<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Repository\ArticleRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ArticlesController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ArticlesController extends DefaultController
{

    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * ArticlesController constructor.
     * @param ArticleRepositoryInterface $articleRepository
     */
    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        return $response->withJson($this->articleRepository->findArticles(), 200);
    }
}
