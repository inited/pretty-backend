<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\ModelGalleryItem;
use App\Model\Entity\UrlEmbeddable;
use App\Model\Repository\ModelRepositoryInterface;
use App\Model\ValueObject\ModelValueObject;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ModelUpdateController
 * @package App\Controllers
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
final class ModelUpdateController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepository;

    /**
     * ModelUpdateController constructor.
     * @param EntityManagerInterface $em
     * @param ModelRepositoryInterface $modelRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        ModelRepositoryInterface $modelRepository
    )
    {
        $this->modelRepository = $modelRepository;
        $this->em = $em;

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws \Exception
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                $data = $request->getParsedBody();
                $entity = $this->modelRepository->findModel(intval($args['id']));

                if (is_array($data) && count($data)) {

                    if (array_key_exists('active', $data)) {
                        $entity->setActive(filter_var($data['active'], FILTER_VALIDATE_BOOLEAN));
                    }

                    if (array_key_exists('buyURL', $data)) {
                        $entity->setBuyUrl((string)($data['buyURL'] ?? ''));
                    }

                    if (array_key_exists('category', $data)) {
                        $entity->setCategory((string)($data['category'] ?? ''));
                    }

                    if (array_key_exists('description', $data)) {
                        $entity->setDescription((string)($data['description'] ?? ''));
                    }

                    if (array_key_exists('model', $data)) {
                        $urlEmbeddable = $entity->getModel();
                        $urlEmbeddable->setUrl((string)($data['model']['url'] ?? ''));
                        $entity->setModel($urlEmbeddable);
                    }

                    if (array_key_exists('gallery', $data)) {
                        $entity->removeModelGalleryItems();
                        foreach ((array)$data['gallery'] as $item) {
                            $urlEmbeddable = new UrlEmbeddable();
                            $urlEmbeddable->setUrl((string)($item['url'] ?? ''));

                            $galleryItem = new  ModelGalleryItem();
                            $galleryItem->setItem($urlEmbeddable);

                            $entity->addModelGalleryItem($galleryItem);
                        }
                    }

                    if (array_key_exists('order', $data)) {
                        $entity->setOrder(intval($data['order']));
                    }

                    if (array_key_exists('picture', $data)) {
                        $urlEmbeddable = $entity->getPicture();
                        $urlEmbeddable->setUrl((string)($data['picture']['url'] ?? ''));
                        $entity->setPicture($urlEmbeddable);
                    }

                    if (array_key_exists('title', $data)) {
                        $entity->setTitle((string)($data['title'] ?? ''));
                    }

                    if (array_key_exists('viewURL', $data)) {
                        $entity->setViewUrl((string)($data['viewURL'] ?? ''));
                    }
                }

                $this->em->flush();

                return $response->withJson(new ModelValueObject($entity), 200);
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(404);
            }
        }

        return $response->withStatus(400);
    }
}
