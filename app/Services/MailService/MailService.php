<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\MailService;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Slim\Views\PhpRenderer;

/**
 * Class MailService
 * @package App\Services\MailService
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class MailService extends PHPMailer implements MailServiceInterface
{

    /** @var PhpRenderer $renderer */
    private $renderer;

    /**
     * MailService constructor.
     * @param PhpRenderer $renderer
     * @param array $settings
     */
    public function __construct(PhpRenderer $renderer, array $settings)
    {
        parent::__construct();
        $this->renderer = $renderer;

        if (!empty($settings['smtp'])) {
            $this->isSMTP();
            $this->Host = $settings['host'];
            $this->Port = $settings['port'];
            $this->SMTPAuth = true;
            $this->Username = $settings['username'];
            $this->Password = $settings['password'];
            $this->SMTPSecure = 'tls';
        }

        if (isset($settings['from'])) {
            $this->From = $settings['from'];
        }

        if (isset($settings['fromName'])) {
            $this->FromName = $settings['fromName'];
        }
    }

    /**
     * @param mixed $address
     * @param mixed $subject
     * @param mixed $template
     * @param array $data
     * @return bool
     */
    public function sendTemplate($address, $subject, $template, array $data = [])
    {
        try {
            $this->addAddress($address);
            $this->Subject = $subject;
            $this->Body = $this->setBodyFromTemplate($template, $data);
            return $this->send();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param mixed $template
     * @param array $data
     * @return mixed
     */
    private function setBodyFromTemplate($template, array $data)
    {
        $this->isHTML();
        return $this->renderer->fetch($template, $data);
    }

}
