<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\FileService;

/**
 * Class FileUploadException
 * @package App\Services\FileService
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class FileServiceException extends \Exception
{

}
