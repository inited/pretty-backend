<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\FileService;

use App\Model\Entity\FileEntity;
use Slim\Http\UploadedFile;

/**
 * Interface FileServiceInterface
 * @package App\Services\FileService
 */
interface FileServiceInterface
{

    /**
     * @param FileEntity $fileEntity
     * @return mixed
     * @throws FileNotFoundException
     */
    public function downloadFile(FileEntity $fileEntity);

    /**
     * @param UploadedFile $file
     * @return FileEntity
     * @throws FileServiceException
     */
    public function uploadFile(UploadedFile $file);
}
