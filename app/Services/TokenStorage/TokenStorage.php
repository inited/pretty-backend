<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\TokenStorage;

use App\Model\Entity\TokenEntity;

/**
 * Class TokenStorage
 * @package App\Services\TokenStorage
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 */
class TokenStorage implements TokenStorageInterface
{

    /**
     * @var null|TokenEntity $token
     */
    private $token;

    /**
     * @return TokenEntity
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param TokenEntity $token
     * @return mixed|void
     */
    public function setToken(TokenEntity $token)
    {
        $this->token = $token;
    }


}
