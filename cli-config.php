<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

require 'vendor/autoload.php';

$settings = include __DIR__ . '/src/settings.php';

if (is_file(__DIR__ . '/src/settings.local.php')) {
    $settingsLocal = require __DIR__ . '/src/settings.local.php';
    $settings = array_replace_recursive($settings, $settingsLocal);
}

$settings = $settings['settings']['doctrine'];

$config = Setup::createAnnotationMetadataConfiguration(
    $settings['meta']['entity_path'],
    $settings['meta']['auto_generate_proxies'],
    $settings['meta']['proxy_dir'],
    $settings['meta']['cache'],
    false
);

$em = EntityManager::create($settings['connection'], $config);

return ConsoleRunner::createHelperSet($em);
