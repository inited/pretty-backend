<?php

return [
    'settings' => [
        'fileFullUrl' => $_ENV['WEB_URL'] . '/api/file/',
        'fileDownloadUrl' => $_ENV['WEB_URL'] . '/api/uploads/',
        'uploadsDirectory' => __DIR__ . '/../public/uploads/',
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'debug' => true,

        'adminEmailForUserApproval' => 'test@test.com',
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Mail
        'mail' => [
            'from' => 'no-reply@test.com',
            'fromName' => 'Test',
            'smtp' => true,
            'host' => '127.0.0.1',
            'port' => 25,
            'username' => 'user',
            'password' => 'password',
        ],

        // Doctrine
        'doctrine' => [
            'connection' => [
                'driver' => 'pdo_mysql',
                'host' => $_ENV['MYSQL_HOSTNAME'],
                'dbname' => $_ENV['MYSQL_DATABASE'],
                'user' => $_ENV['MYSQL_USER'],
                'password' => $_ENV['MYSQL_PASSWORD'],
                'charset' => 'utf8',
                'driverOptions' => array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            ],
            'meta' => [
                'entity_path' => [
                    'app/Model/Entity/'
                ],
                'auto_generate_proxies' => true,
                'cache' => null,
                'proxy_dir' => __DIR__ . '/../temp/cache/proxies',
                'customHydrationModes' => \Doctrine\ORM\Query::HYDRATE_ARRAY,
            ],
        ],
    ],
];

function isSecure()
{
    return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
}
