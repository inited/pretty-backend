<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Routes
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @var \Slim\App $app
 * @var \Psr\Container\ContainerInterface $container
 */
$app->options('/{routes:.+}', function (Request $request, Response $response, array $args) {
    return $response;
});

// CORS
$app->add(function (Request $req, Response $res, App $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

// API
$app->group('/api', function () use ($container) {
    // Articles
    $this->get('/article/{id}', 'ArticleController:defaultAction');
    $this->put('/article/{id}', 'ArticleUpdateController:defaultAction');
    $this->get('/articles', 'ArticlesController:defaultAction');
    $this->post('/article', 'ArticleCreateController:defaultAction');

    // Contests
    $this->delete('/contest/{id}', 'ContestDeleteController:defaultAction');
    $this->get('/contest/{id}', 'ContestController:defaultAction');
    $this->put('/contest/{id}', 'ContestUpdateController:defaultAction');
    $this->get('/contests', 'ContestsController:defaultAction');
    $this->post('/contest', 'ContestCreateController:defaultAction');
    $this->get('/actualContest', 'ContestActualController:defaultAction');
    $this->post('/actualContest/results', 'ContestActualController:resultsAction');
    $this->get('/contest/{id}/results', 'ContestController:resultsAction');

    // Files
    $this->get('/file/{id}', 'FileController:defaultAction');
    $this->post('/file/upload', 'FileUploadController:defaultAction');

    // Models
    $this->delete('/model/{id}', 'ModelDeleteController:defaultAction');
    $this->get('/model/{id}', 'ModelController:defaultAction');
    $this->put('/model/{id}', 'ModelUpdateController:defaultAction');
    $this->get('/models', 'ModelsController:defaultAction');
    $this->post('/model', 'ModelCreateController:defaultAction');

    // Notifications
    $this->delete('/notification/{id}', 'NotificationDeleteController:defaultAction');
    $this->get('/notifications', 'NotificationsController:defaultAction');
    $this->get('/notifications/register', 'NotificationsRegisterController:defaultAction');
    $this->post('/notifications/send', 'NotificationsSendController:defaultAction');
});
